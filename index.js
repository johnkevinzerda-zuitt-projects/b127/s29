const express = require('express');
//create an application using express
//This creates an express application and stores this as a constant called app.
//app is our server
const app = express()
//for our application to run, we need a port to listen to
const port = 3000;

//Setup for allowing the server to handle data from requests
//Allows your app to read JSON data
//Methods used from express JS are middlewares
//Middleware is a software that provides services outside of what's offered by the operating system
app.use(express.json());
//allows your app to read data from forms
//By default, information received from the url can only be received as a string or an array
//By applying the option of "extended:true" this allows us to receive information in other data types such as an object/boolean etc, which we will use throughout our application
app.use(express.urlencoded({extended:true}))



//We will create routes
//Express has methods corresponding to each HTTP method
//This route expects to receive a GET request at the base URI '/'
//
app.get('/', (req, res)=> {
	//res.send uses the express JS module's method instead to send a response back to the client
	res.send("Hello World")
})

app.get('/hello',(req,res)=>{
	res.send('Hello from the /hello endpoint!')
})


//Post method
app.post('/hello',(req,res)=>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

//for /signup
//mock database
let users = [];

app.post('/signup',(req,res)=>{
	console.log(req.body)
	//if contents of the request body with the property "username" and "password" is not empty
	if (req.body.username != '' && req.body.password != '') {
		//This will store the user object sent via POSTMAN to the users array created above
		users.push(req.body)
		//send response
		res.send(`User ${req.body.username} successfully registered!`)
	} else {
		res.send('Please input BOTH username and password')
	}
})

//Update the password of a user that matches the information provided in the client/POSTMAN
app.put('/change-password',(req,res)=>{
	let message;
	//create a for loop that will loop through the elements of the "users" array
	for (let i = 0; i < users.length; i++) {
		if (req.body.username == users[i].username) {
		//Changes the password of the user found by the loop into the password provided in the client/POSTMAN
		users[i].password = req.body.password

		//Changes the message to be sent back by the response

		message = `User ${req.body.username}'s password has been updated`
		break;

		}else{
			message = 'user does not exist'
		}
	}
	res.send(message)
})



/*s29 Activity*/

//Create a GET route that will access the "/home" route that will print out a simple message.
app.get('/home',(req,res)=>{
	res.send('Welcome to the homepage')
})


//Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
app.get('/users',(req,res) =>{
	res.send(users)
})


//Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.


app.delete('/delete-user',(req,res) =>{
	let alert;
	if (users.length == 0) {
		alert = `user does not exist`
	}else{
		for (let i =0; i < users.length; i++) {
			if (req.body.username != users[i].username) {
				alert =`user "${req.body.username}" does not exist`
			} else {
				users.splice(i,1)
				alert = `user ${req.body.username} has been deleted.`

			}
		}
	}

	res.send(alert)
})
app.listen(port, () => console.log(`Server is running @ port ${port}`))